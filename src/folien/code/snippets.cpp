int zahlen[10];
int zahlen2[] = { 3,4,5 };
char wort1[] = {'H','a','l','l','o','\0'};
char wort2[] = "Welt";

if (BEDINGUNG) { CODE } else { CODE }

if (x < y) { cout << "x < y"; }
else { cout << "y <= x"; }

if (x < y) { cout << "x < y"; }
else if (y < x) { cout << "y < x"; }
else { cout << "y == x"; }

for (INITIALISIERUNG;BEDINGUNG;INKREMENT) {
    CODE
}

for (int i=0;i<x;i++) {
	cout << i;	
}

while (BEDINGUNG) {
    CODE
}

while (x!=1) {
	cout << x;
	x--;	
}

for (int i=0;i<x;i++) {
    cout << i;	
}

unsigned long int x;

int i=42;
cout << "Ihre Lieblingszahl: " << i;

int i=42;
int j=77;
cout << i << " ist eine Zahl, " << j << " eine andere.");

cout << STRING << v1 << v2 << ...

int i;
scanf("%d",&i);

int i;
int j;
scanf("%d %d",&i,&j);

char s[100];
scanf("%s",s);

char s[100];
char t[100];
scanf("%s",s);
scanf("%s",t);
		  
char s[100];
char t[100];
scanf("%s %s",s,t);

char s[100];
scanf("%[^\n]",s);

int i;
scanf("%d",&i);
char c;
scanf("%c",&c);
printf("%d %c \n",i,c);

int i;
scanf("%d",&i);
while ( getchar() != '\n' ) {}
char c;
scanf("%c",&c);
printf("%d %c \n",i,c);

int * x;
void foo(int * x) { ... }

int x = 3;	int * y = &x;
printf("%d",*y);

enum [Typname] {
    Bezeichner [= Wert],
    Bezeichner [= Wert],
    ...
};

enum getraenke {
    COLA, FANTA, SPRITE,
    BIER, MILCH
};

int main() {
    enum getraenke g = COLA;
    cout << g << endl;
    cout << g+1 << endl);
        
    return 0;
}

enum getraenke g = 42;

for (enum getraenke h = COLA; h <= MILCH; h++) {
    cout << h << endl;
}

typedef [ALTER_DATENTYP] [NEUER_NAME];


typedef int * intptr;
typedef complex * complexptr;
	
int main() {
    int x = 42;
    intptr p = &x;
}

if (x == 42) {
    cout << "Good choice!" << endl;
}
else {
    cout << "Fail!" << endl;
}

cout << x==42 ? "Yay!\n" : "Boo!\n" << endl;

if (...) {
} else if (...) {
} else if (...) {
} else if (...) {
} ...

switch (AUSDRUCK) {
    case WERT_1: BEFEHLE_1 ;
    case WERT_2: BEFEHLE_2 ;
    case WERT_3: BEFEHLE_3 ;
    default: BEFEHLE_4 ;
}

#ifndef TICTACTOE_H
#define TICTACTOE_H

	...

#endif

int a = 42;
int * b = a;
b++;
cout << *b // Krawumm!!

int *a = nullptr;
cout << a // s.o.

using  [NEUER_NAME] = [ALTER_DATENTYP];

using intptr = int*;
	
int main() {
    int x = 42;
    intptr p = &x;
}
