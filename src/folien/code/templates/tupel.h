#ifndef TUPEL_H
#define TUPEL_H

#include<string>
#include<iostream>

template<class T>
class tupel {
protected:
    T _x,_y;
    
public:
    tupel(T x_, T y_) : x{x_}, y{y_} {}
    
    virtual std::string str()
    { 
        return "(" + std::to_string(x) + 
               "," + std::to_string(y) + ")";
    }
};

template<class T>
std::ostream & operator<<(
    std::ostream & s,
    tupel<T> & t)
{
    s << t.str();
    return s;
}

#endif

