#include<iostream>
using namespace std;

int main() {
    int x;
    cout << "Bitte eine Zahl eingeben: ";
    cin >> x;
    
    switch(x) {
        case 42: cout << "Good choice\n"; break;
        case 23: cout << "OK\n"; break;
        default: cout << "Fail\n";
    }
    
    cout << x;
    switch(x<0) {
        case (1): cout << " ist negativ!\n"; break;
        case (0): cout << " ist positiv!\n"; break;
    }
    
    cout << x << " ist " << (x<0?"negativ":"positiv") << endl;
}
