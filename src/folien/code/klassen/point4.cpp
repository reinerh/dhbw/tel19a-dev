#include<iostream>
#include<string>
#include<sstream>
using namespace std;

class Point
{
private:
    float x;
    float y;

public:
    Point(Point p);
    Point(float x_, float y_);
    void move(float dx, float dy);
    string str();
};

Point operator+(Point const& l, Point const& r);
ostream & operator<<(ostream & l, Point const& r);

int main() {
    Point p{3,4};
    p.move(2.5, 1.8);
    cout << p.str() << endl; 
    
    return 0;
}

Point(Point p)
    : Point(p.x, p.y)
    {}

Point::Point(float x_, float y_)
    : x{x_}, y{y_}
    {}

void Point::move(float dx, float dy)
{
    x += dx;
    y += dy;
}

string Point::str()
{
    stringstream s;
    s << "(" << x << "," 
      << y << ")";
    return s.str();
}

Point operator+(Point const& l, Point const& r)
{
    Point ergebnis = l;
    ergebnis.move(r);
    return ergebnis;
}

ostream & operator<<(ostream & l, Point const& r)
{
    return l << r.str();
}

