#include<iostream>
using namespace std;

void foo(int k) {
	if (k == 0) { return; }
	cout << k << endl;
	foo(k-1);
}

int add(int x, int y) {
    if (y == 0) return x;         // x + 0 = x
    else return add (x,y-1) + 1;  // x + (y + 1) = (x + y) + 1
}

int factorial_it(int n) {
    int res = 1;
    for (int i=1;i<=n;i++) {
   	    res *= i;
    }
    return res;
}

int factorial_rec1(int n) {
    if (n==0) return 1;
    return n*factorial_rec1(n-1);
}

int factorial_rec2(int n) {
    return n==0?1:n*factorial_rec2(n-1);
}


int main() {
    foo(42);
    
    cout << "add(3,4) == " <<  add(3,4) << endl;
    
    cout << "Fakultaet von 5:\n";
    cout << "Iterativ: " << factorial_it(5) << endl;
    cout << "Rekursiv 1: " << factorial_rec1(5) << endl;
    cout << "Rekursiv 2: " << factorial_rec2(5) << endl;
    
    return 0;
}
